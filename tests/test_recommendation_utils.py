import pytest
import datetime
from utils.recommendation_util import RecommendationUtils


@pytest.mark.django_db
class TestRecommationUtils:
    def test_create_energy_bill_date(self):
        month, year = 1, 2023
        energy_bill, _, _ = RecommendationUtils.create_energy_bill_date_info(month, year)

        assert isinstance(energy_bill, dict)
        assert energy_bill.keys() == {'month', 'year', 'energy_bill'}

    def test_create_energy_bill_date_handles_invalid_month(self):
        invalid_months = [-1, 0, 13]
        for invalid_month in invalid_months:
            year = 2023
            with pytest.raises(ValueError,match=f'Invalid month: {invalid_month}'):
                RecommendationUtils.create_energy_bill_date_info(invalid_month, year)

    def test_create_energy_bill_date_handles_invalid_year(self):
        invalid_years = [1999, 1890]
        for invalid_year in invalid_years:
            month = 1
            with pytest.raises(ValueError,match=f'Invalid year: {invalid_year}'):
                RecommendationUtils.create_energy_bill_date_info(month, invalid_year)

    def test_create_energy_bill_date_handles_current_month_and_year(self):
        #  Caso para quando o mes nao é 1
        month = 2
        year = 2023

        energy_bill, _, _ = RecommendationUtils.create_energy_bill_date_info(month, year)

        assert energy_bill['month'] == 1
        assert energy_bill['year'] == year

        # Caso para quando o mes é 1
        month = 1
        year = 2023

        energy_bill, _, _ = RecommendationUtils.create_energy_bill_date_info(month, year)

        assert energy_bill['month'] == 12
        assert energy_bill['year'] == year - 1